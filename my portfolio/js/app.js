/****
Navbar Animation 
****/
const mainHeader = document.querySelectorAll(".main-header");
const contactHeader = document.querySelectorAll(".main-body__content--contact-block__header");
const expertHeader = document.querySelectorAll(".main-body__content--expertise-block__header");
const techHeader = document.querySelectorAll(".main-body__content--technology-block__header");
const sectionOne = document.querySelector(".main-body__content--landing");
const faders = document.querySelectorAll(".header");


const appearOptions = {
    threshold: 0,
    rootMargin: "0px 0px -50px 0px"
};

const appearOnScroll = new IntersectionObserver(function(
        entries,
        appearOnScroll
    ) {
        entries.forEach(entry => {
            if (!entry.isIntersecting) {
                return;
            } else {
                entry.target.classList.add("appear");
                appearOnScroll.unobserve(entry.target);
            }

        });
    },
    appearOptions);

/****
Fading Animation 
****/



faders.forEach(fader => {
    appearOnScroll.observe(fader);
});


mainHeader.forEach(fader => {
    appearOnScroll.observe(fader);
});


contactHeader.forEach(fader => {
    appearOnScroll.observe(fader);
});

expertHeader.forEach(fader => {
    appearOnScroll.observe(fader);
});

techHeader.forEach(fader => {
    appearOnScroll.observe(fader);
});

/****
Sliding Animation 
****/
const aboutIntro = document.querySelectorAll(".about-intro");
const aboutAvatar = document.querySelectorAll(".about-avatar");
const techSliders = document.querySelectorAll(".images");
const imgSliders = document.querySelectorAll(".col-md-4");

aboutIntro.forEach(slider => {
    appearOnScroll.observe(slider);
});


aboutAvatar.forEach(slider => {
    appearOnScroll.observe(slider);
});

imgSliders.forEach(slider => {
    appearOnScroll.observe(slider);
});

techSliders.forEach(slider => {
    appearOnScroll.observe(slider);
});

/****
Page Navigation Scroll
****/

function smoothScroll(target, duration) {
    var target = document.querySelector(target);
    var targetPosition = target.getBoundingClientRect().top;
    var startPosition = window.pageYOffset;
    var distance = targetPosition - startPosition;
    var startTime = null;

    function animation(currentTime) {
        if (startTime === null)
            startTime = currentTime;
        var timeElapsed = currentTime - startTime;
        var run = ease(timeElapsed, startPosition, distance, duration);
        window.scrollTo(0, run);
        if (timeElapsed < duration)
            requestAnimationFrame(animation);

    }

    function ease(t, b, c, d) {
        t /= d;
        return c * t * t * t + b;
    };
    requestAnimationFrame(animation);
}

var learnSection = document.querySelector(".btn");

learnSection.addEventListener("click", () => {
    smoothScroll(".main-body__content--about-block", 100);

});

// var aboutSection = document.querySelector(".about");

// aboutSection.addEventListener("click", () => {
//     smoothScroll(".main-body__content--about-block", 200);

// });

// var expSection = document.querySelector(".technology");

// expSection.addEventListener("click", () => {
//     smoothScroll(".main-body__content-technology-block", 200);

// });

// var servSection = document.querySelector(".expertise");

// servSection.addEventListener("click", function() {
//     smoothScroll(".main-body__content--expertise-block", 200);

// });

// var contactSection = document.querySelector(".contact");

// contactSection.addEventListener("click", function() {
//     smoothScroll(".main-body__content--contact-block", 200);

// });



/***
 Back to Top Button
 ***/

const backTop = document.querySelector(".top-button");

window.addEventListener("scroll", () => {
    if (window.pageYOffset > 300) {
        if (!backTop.classList.contains("btn-entrance")) {
            backTop.classList.remove("btn-exit");
            backTop.classList.add("btn-entrance");
            backTop.style.display = "flex";
        }
    } else { // Hide backToTopButton
        if (backTop.classList.contains("btn-entrance")) {
            backTop.classList.remove("btn-entrance");
            backTop.classList.add("btn-exit");
            setTimeout(function() {
                backTop.style.display = "none";
            }, 250);
        }
    }

});

backTop.addEventListener("click", () => {
    window.scroll(0, 0);
});

/***
 SVG Animation
 ***/

const profileImage = document.querySelector(".profile-image");
const lineOne = profileImage.getElementById("lineOne");
const lineTwo = profileImage.getElementById("lineTwo");
const lineThree = profileImage.getElementById("lineThree");
const lineFour = profileImage.getElementById("lineFour");
const lineFive = profileImage.getElementById("lineFive");
const lineSix = profileImage.getElementById("lineSix");
const lineSeven = profileImage.getElementById("lineSeven");
const lineEight = profileImage.getElementById("lineEight");
const handOne = profileImage.getElementById("handOne");
const handTwo = profileImage.getElementById("handTwo");

const lines = [lineOne, lineTwo, lineThree, lineFour, lineFive, lineSix, lineSeven, lineEight];

const hands = [handOne, handTwo];

const tlm = new TimelineMax({});
tlm
    .from('.header-desc', { opacity: 0, duration: 1, x: -50 }, 0)
    .from('.profile-avatar', { opacity: 0, duration: 1, x: 90 }, 0)
    .from('.btn', { opacity: 0, duration: 1 });


profileImage.addEventListener("load", () => {
    tlm
        .from(lines, { opacity: 0, x: -14, repeat: -1, stagger: 0.25, }, 0.125)
        .to(hands, 0.15, {
            y: -5,
            rotate: 15,
            transformOrigin: "50% 50%",
            repeat: -1,
            stagger: 0.15,
        }, 0.125);
});