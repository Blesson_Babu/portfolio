# Portfolio 

Both web design(master) and UI(web design) Design available

## Installation

1. HTML and CSS based code web design.
2. "cdn" used for basic bootstrap inclusions. 
3. "gulp.js" used for the the Sass implementation.
4. Please checkout admin panel design for Sass Implementation

## Projects
1. Portfolio the basic static web page made using Sass and gulp index.  
2. Folowed by the project handled by myself during the training period.
3. UI designs are commited using Adobe XD and illustrator.


### Contributing
1. Pull requests are welcome. 
2. For major changes, please open an issue first to discuss what you would like to change.

#### License
©2021-Blesson S. Babu